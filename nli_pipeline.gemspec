require File.expand_path('lib/nli_pipeline/version', __dir__)

Gem::Specification.new do |s|
  s.name         = 'nli_pipeline'
  s.version      = NliPipeline::VERSION
  s.date         = '2018-04-26'
  s.summary      = 'Pipeline manager for NLireland'
  s.description  = 'A gem for manging .EXAMPLE files'
  s.authors      = ['Conor Sheehan']
  s.email        = 'csheehan@nli.ie'
  s.files        = Dir.glob('lib/**/*.rb')
  s.test_files   = s.files.grep(%r{^(spec)/})
  s.require_path = 'lib'
  s.executables  = ['setup_pipeline']
  s.license      = 'MIT'
  s.homepage     = 'https://rubygems.org/gems/nli_pipeline'
  s.metadata     = { 'source_code_uri' => 'https://bitbucket.org/nlireland/nli-pipeline-gem' }
  s.add_runtime_dependency('colorize', ['~>0.8', '>=0.8.1'])
  s.add_development_dependency('byebug', '~> 10.0')
  s.add_development_dependency('rspec', ['~>3.7', '>=3.7.0'])
  s.add_development_dependency('rubocop', '~> 0.57.2')
  s.add_development_dependency('simplecov', '~> 0.16.1')
  s.add_development_dependency('yard', '~> 0.9.14')
end
