require 'colorize'
require 'nli_pipeline/system_wrapper/call_wrapper'
require 'nli_pipeline/abstract_util/init_attrs'

module NliPipeline
  # TODO: enforce that command must be either cp or mv

  # Manage example files
  #
  # EXAMPLE:
  #   >> NliPipeline::FileManager.new.copy_example_files()
  #   => Moving 1 .EXAMPLE files
  class FileManager < SystemWrapper::CallWrapper
    include AbstractUtil

    # static methods required by NliPipeline::AbstractUtil::init_attrs
    # @see NliPipeline::AbstractUtil#init_with_attrs
    # @see NliPipeline::AbstractUtil#get_allowed_args
    # @return [Hash]
    def self.supported_args
      {
        path: '', extension: '.EXAMPLE', debug: false,
        fail_on_error: false, log_dir: '.pipeline_logs',
        created_get_log_name: 'created_files_', backup_path: ''
      }
    end

    # @see NliPipeline::AbstractUtil#init_with_attrs
    # @see NliPipeline::AbstractUtil::ClassMethods#required_args?
    # @return [Array[Symbol]]
    def self.required_args
      [:path]
    end

    # automatically set backup path
    # init_with_attrs handles the rest
    # @param kwargs [Hash] all keyword (only supported_args) will be added as vars
    def initialize(**kwargs)
      # backup_path should not be configurable
      # set it directly regardless of whether it's passed in
      kwargs[:backup_path] = "#{kwargs[:path]}/**/*.backup*"
      init_with_attrs(**kwargs)
    end

    # @return [Array[String]] all example files under @path
    def all_example_files
      example_file_path = "#{@path}/**/*#{@extension}"
      example_files = Dir.glob(example_file_path)
      if example_files.empty?
        raise ArgumentError, "No #{@extension} Files found at #{example_file_path}"
      end
      puts("Moving #{example_files.count} #{@extension} files".green)
      example_files
    end

    # read latest log in .pipeline_logs
    # @return [String] files created by last setup_pipeline
    def last_created_files
      logs = Dir.glob("#{@path}/#{@log_dir}/*")
      if logs.empty?
        puts("No logs found in #{@path}/#{@log_dir}".red)
        return false
      end
      log_numbers = logs.map { |x| x.split(@created_get_log_name.to_s).last }
      latest_log_number = log_numbers.uniq.min
      File.readlines(get_log_name(latest_log_number))
    end

    # copy all files ending in @extension under @path.
    # if a non-exmaple version of the file exists, back it up.
    # @param command       [String]        command to run (cp/mv)
    def copy_example_files(command = 'cp')
      time_stamp = Time.now.strftime('%Y%m%d%H%M')
      puts("Setting up pipeline in #{@path}".yellow)
      example_files = all_example_files

      backup_non_example_files(example_files, time_stamp)

      example_files.each do |example_file|
        target_file = example_file.gsub(@extension, '')
        call_system("#{command} #{example_file} #{target_file}")

        if File.exist?(target_file)
          add_to_log(target_file, time_stamp)
          puts("\t add #{target_file} to log #{get_log_name(time_stamp)}") if @debug
        end
      end
    end

    # get backup dates in order from newest to oldest
    def all_backups
      backup_files = Dir.glob(@backup_path)
      backup_numbers = backup_files.map { |x| x.split('.backup').last }
      backup_numbers.uniq.sort
    end

    # for each example file,
    # if a non-example version of that file exists,
    # back it up.
    #
    # @param example_files [Array[string]] list of example files
    # @param time_stamp    [String]        time of backup
    # @param command       [String]        command to run (cp/mv)
    def backup_non_example_files(example_files, time_stamp, command = 'cp')
      possible_files = example_files.map { |f| f.gsub(@extension, '') }
      files_to_backup = possible_files.select { |file| File.exist?(file) }
      # raise ArgumentError.new("#{files_to_backup.to_s} #{example_files.to_s}")
      return false if files_to_backup.empty?

      # preset backup time so all backups fomr same batch have the same timestamp
      puts("Backing up #{files_to_backup.count} files".green)
      files_to_backup.each do |file_to_backup|
        call_system("#{command} #{file_to_backup} #{file_to_backup}.backup#{time_stamp}")
      end
    end

    # @param command [String] system command.
    #   use mv to remove backup,
    #   use cp to keep backup.
    # @return Boolean
    def load_from_backup(command = 'cp')
      puts("loading backup in #{@path}".yellow)
      backups = all_backups
      if backups.empty?
        puts("No backups found in #{@backup_path}".red)
        return false
      end

      backup_path, backup_date = handle_backup_user_input(backups)
      files_to_load = Dir.glob(backup_path)
      puts "loading #{files_to_load.count} files from #{backup_path}".green

      files_to_load.each do |backup_file|
        target_file = backup_file.gsub("\.backup#{backup_date}", '')
        call_system("#{command} #{backup_file} #{target_file}")
      end
    end

    # keeps asking the use to choose a backup
    # until they choose a valid one
    # @param backups [Array[String]]
    # @return [Array[String]]
    def handle_backup_user_input(backups)
      puts "which backup would you like to load?\n#{backups}".yellow
      backup_date = STDIN.gets.chomp

      until backups.include?(backup_date)
        puts "#{backup_date} is not in backups".red
        puts "please choose from: #{backups}".yellow
        backup_date = STDIN.gets.chomp
      end
      ["#{@path}/**/*.backup#{backup_date}", backup_date]
    end

    private

    # create dir for pipeline logs
    def create_log_dir
      full_path_to_log = "#{@path}/#{@log_dir}"
      Dir.mkdir(full_path_to_log) unless File.directory?(full_path_to_log)
    end

    # @param date [String]
    # @return [String] full path to log
    def get_log_name(date)
      "#{@path}/#{@log_dir}/#{@created_get_log_name}#{date}"
    end

    # appends to log file
    # @param line [String] line to append to log file
    # @param date [String]
    def add_to_log(line, date)
      create_log_dir # if log doesn't exist
      File.open(get_log_name(date), 'a') { |file| file.write(line) }
    end
  end
end
