module NliPipeline
  # constant use in nli_pipeline.gemspec and bin/setup_pipeline
  # to determine current version of gem
  # should also match tag on bitbucket
  VERSION = '0.2.0'.freeze
end
