require 'optparse' # core gem, no install required
# require 'byebug'

module NliPipeline
  # TODO: replace $PWD with arg to optionally pass directory
  # and use Dir.pwd by default
  # https://stackoverflow.com/questions/1937743/how-to-get-the-current-working-directorys-absolute-path-from-irb

  # Class used to parse arguments from bin/setup_pipeline executable
  class SetupPipeline
    attr_reader :commands, :docker_commands, :dir_error, :options, :version

    # Set up class attributes
    def initialize
      @version = NliPipeline::VERSION

      @commands = {
        undo:              proc { |fm| fm.load_from_backup },
        show_last_created: proc { |fm| puts(fm.last_created_files.to_s) }
      }

      @docker_commands = {
        docker_build:          proc { |dm| dm.build },
        docker_build_commit:   proc { |dm| dm.build_commit? },
        docker_build_and_save: proc { |dm| dm.build_and_save },
        docker_deploy_branch:  proc { |dm| dm.deploy_branch },
        docker_deploy_master:  proc { |dm| dm.deploy_master },
        docker_exec:           proc { |dm| dm.docker_exec },
        docker_run:            proc { |dm| dm.run },
        docker_save:           proc { |dm| dm.save }
      }

      @dir_error = "
        The first argument to setup_pipeline must be a valid directory.
        For example, setting up a pipeline in the current directory:

          setup_pipeline $PWD

        The exceptions to this are:\n
          setup_pipeline --version
          setup_pipeline --help
          setup_pipeline --show-commands
          setup_pipeline --show-flags
      "
    end

    # simple formatter
    def show_commands
      puts('FileManager commands')
      puts(@commands.keys)
      puts('DockerManager commands')
      puts(@docker_commands.keys)
    end

    # print help screen to console
    # @param opts [OptionParser]
    def help(opts)
      how_to_use = "HOW TO USE:\n
        setup_pipeline $PWD

        #{opts}

        FileManager commands:   #{@commands.keys}

        DockerManager commands: #{@docker_commands.keys}

      "

      errors = "COMMON ERRORS:\n
        #{@dir_error}
      "

      docs = "FOR MORE INFORMATION PLEASE READ THE DOCS:\n
        on bitbucket:\t\thttps://bitbucket.org/nlireland/nli-pipeline-gem\n
        or on ruby gems:\t\thttps://rubygems.org/gems/nli_pipeline\n
      "

      puts("#{how_to_use}\n#{errors}\n#{docs}")
    end

    # Commandline Flags:
    #   $1 (file path) (required)
    #   --version
    #   --extension=(string: file extension)
    #   --debug (show all system commands when enabled)
    def parse_flags
      options = {}
      OptionParser.new do |opts|
        @options = opts
        # break out of function, don't throw error about passing directory path
        opts.on('-h', '--help') do |_v|
          help(opts)
          return false
        end
        opts.on('--show-commands') do |_v|
          show_commands
          return false
        end
        opts.on('--show-flags') do |_v|
          puts(self.options)
          return false
        end
        opts.on('-v', '--version') do |_v|
          puts(self.version)
          return false
        end
        opts.on('-cmds=', '--commands=a,b,c', Array, 'Array of bash commands') do |v|
          options[:bash_commands] = v
        end
        opts.on('--debug') do |v|
          options[:debug] = v
        end
        opts.on("--extension=[[\w\.]+]", String, 'String file extension') do |v|
          options[:extension] = v
        end
        # override git branch (for docker deploy)
        opts.on("--git-branch=[[\w\.]+]", String, 'String git branch') do |v|
          options[:git_branch] = v
        end
        opts.on('--fail-on-error') do |v|
          options[:fail_on_error] = v
        end
        opts.on("--image=[([\w\-\.\:]+)]", String, 'String docker image') do |v|
          options[:image_name] = v
        end
        opts.on("--mount=[([\w\-\.\:]+)]", String, 'String directory to mount') do |v|
          options[:mount] = v
        end
        opts.on('--ports=a,b,c', Array, 'Array of ports') do |v|
          options[:ports] = v
        end
        opts.on("--proxy=[([\w\-\.\:]+)]", String, 'String proxy') do |v|
          options[:proxy] = v
        end
        opts.on("--upstream-image=[([\w\-\.\:]+)]", String, 'String dockerhub image') do |v|
          options[:upstream_image_name] = v
        end
      end.parse!
      options
    end

    # Commandline Arguments:
    #   $1 (file path) (required)
    #   $2 (command to run)
    #
    # @raise [ArgumentError] if $1 is missing or is not a valid directory
    def parse_args
      raise ArgumentError, "\n#{@dir_error}\n" unless ARGV[0] && Dir.exist?(ARGV[0])

      args = {}
      args[:path] = ARGV[0]
      args[:command] = ARGV[1].to_sym unless ARGV[1].nil? || ARGV[1].empty?
      args
    end

    # handle case where invalid flag is passed
    # show user help screen
    def flags_or_help_screen
      begin
        flags = parse_flags
      rescue OptionParser::InvalidOption => e
        puts(e.to_s.red)
        # inject --help as first arg
        ARGV.insert(0, '--show-flags')
        #  call flags again to display help dialogue
        parse_flags
        # re-throw error
        raise e
      end
      flags
    end

    # handle what manager object to create based on flags / command
    # @param flags   [Hash[Symbol: String]]
    # @param command [String]
    def delegate_to_managers(flags, command)
      if @docker_commands.key?(command)
        # always fail on error if anything goes wrong with a deploy
        flags[:fail_on_error] = true if command.to_s.include?('deploy')
        dm = NliPipeline::DockerManager.new(**flags)
        @docker_commands[command].call(dm)
      # if not DockerManager, use FileManager
      else
        fm = NliPipeline::FileManager.new(**flags)
        if @commands.key?(command)
          @commands[command].call(fm)
        # is no command is specified, copy example files
        else
          fm.copy_example_files
        end
      end
    end

    # TODO
    # move undo arg handling to parse_flags?
    # split out main into handler for FileManager and DockerManager

    # Method called by bin/setup_pipeline
    # Parse commands (command line arguments) and pass flags / flags to parse_flags
    def main
      # byebug
      flags = flags_or_help_screen
      # flags will be false if a flag that exits early is passed e.g. false
      # otherwise flags will be some kind of hash, all of which are truthy, even {}
      return false unless flags

      # TODO
      # document this better or
      # get rid of commands? use separate executables in bin for FileManager vs DockerManager?

      # command is the second argument
      # e.g. setup_pipeline $PWD COMMAND_GOES_HERE --flags --go --here
      args = parse_args
      flags[:path] = args[:path]
      command = args[:command]

      delegate_to_managers(flags, command)
    end
  end
end
