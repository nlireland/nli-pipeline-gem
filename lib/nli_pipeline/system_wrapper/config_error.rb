require 'nli_pipeline/abstract_util/init_attrs'

module NliPipeline
  module SystemWrapper
    # handles errors for config
    class ConfigError < StandardError
      attr_reader :error_message

      include NliPipeline::AbstractUtil

      # static methods required by NliPipeline::AbstractUtil::init_attrs
      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil#get_allowed_args
      # @return [Hash]
      def self.supported_args
        { config: {}, msg: 'Config Error', file: false }
      end

      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil::ClassMethods#required_args?
      # @return [Array[Symbol]]
      def self.required_args
        [:config]
      end

      # override to_s from AbstractUtil
      # return single line summary of error
      # @return [String]
      def to_s
        @error_message
      end

      # wrap up kwargs into single variable error_message
      def initialize(**kwargs)
        init_with_attrs(**kwargs)
        message = "#{@msg}: #{@config}"
        message += " in #{@file}" if file
        @error_message = message
        super(message)
      end
    end
  end
end
