require 'open3'
require 'nli_pipeline/system_wrapper/call_wrapper_error'
require 'nli_pipeline/abstract_util/init_attrs'

module NliPipeline
  # methods / classes that interact with the os
  module SystemWrapper
    # wrapper for system calls
    # handles output / debugging
    class CallWrapper
      include NliPipeline::AbstractUtil

      attr_accessor :last_return_code

      # static methods required by NliPipeline::AbstractUtil::init_attrs
      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil#get_allowed_args
      # @return [Hash]
      def self.supported_args
        { debug: false, fail_on_error: false }
      end

      # no required args in this case, but method is required
      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil::ClassMethods#required_args?
      # @return [Array]
      def self.required_args
        []
      end

      # @see NliPipeline::AbstractUtil#init_with_attrs handles everything
      def initialize(**kwargs)
        init_with_attrs(**kwargs)
      end

      # @param command              [String]  command to run
      # @param custom_error_message [String]  custom error to display on error
      # @param return_output        [Boolean] if false return code, if true return stdout
      # @return [String | Boolean]
      def call_system(command, custom_error_message: false, return_output: false)
        result = return_output ? `#{command}`.chomp : system(command)
        return_code = $CHILD_STATUS
        if @debug
          puts("\t#{command}")
          puts("\treturned: #{result} #{return_code}")
        end
        # can't return both output and code in return_output case
        # so assign instance var
        @last_return_code = return_code
        # only catches cases where command finished but with non-zero exit code
        # e.g. "echo 'hi" will throw an exception regardless of @fail_on_error
        # "echo 'hi' grep | 'y'" will throw a CallWrapperError only if @fail_on_error is true
        if @fail_on_error && !result
          puts(custom_error_message.red) if custom_error_message
          raise CallWrapperError.new(call: command, code: return_code)
        end
        result
      end

      # # on hold until we figure out how to test Open3.popen3 better
      # # expect object to receive_message_chain ?
      # def call_system(command, custom_error_message: false, return_output: false)
      #   # result = system(command)
      #   Open3.popen3(command) do |stdin, stdout, stderr, wait_thr|
      #     # return comes back as pid #{int} exit #{int}
      #     return_code = wait_thr.value.to_s.split(' ')[-1].to_i
      #     if @debug
      #       puts("\t#{command}")
      #       puts("\treturned: #{result} #{return_code}")
      #     end
      #     # only catches cases where command finished but with non-zero exit code
      #     # e.g. "echo 'hi" will throw an exception regardless of @fail_on_error
      #     # "echo 'hi' grep | 'y'" will throw a CallWrapperError only if @fail_on_error is true
      #     if @fail_on_error && return_code != 0
      #       puts(custom_error_message.red) if custom_error_message
      #       raise CallWrapperError.new(call: command, code: return_code)
      #     end
      #     return stdout.gets if return_output
      #     # return true if command exits with 0, otherwise return false
      #     return_code == 0
      #   end
      # end

      private

      # pretty print intended for use with @see #call_system
      # @param custom_message [String]
      def pretty_print(custom_message: false)
        # assign return value of block to ret
        ret = yield
        out_str = ret.to_s

        message_args = { custom_message: custom_message, out_str: out_str }
        out_str = format('%<custom_message>-20s: %<out_str>s', message_args) if custom_message
        puts(ret ? out_str.green : out_str.red)
        ret
      end
    end
  end
end
