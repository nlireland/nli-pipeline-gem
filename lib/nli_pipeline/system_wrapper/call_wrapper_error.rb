require 'nli_pipeline/abstract_util/init_attrs'

module NliPipeline
  module SystemWrapper
    # handles errors for system calls
    class CallWrapperError < StandardError
      include NliPipeline::AbstractUtil

      # static methods required by NliPipeline::AbstractUtil::init_attrs
      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil#get_allowed_args
      # @return [Hash]
      def self.supported_args
        { call: '', code: 0, msg: 'An error occurred during system call' }
      end

      # @see NliPipeline::AbstractUtil#init_with_attrs
      # @see NliPipeline::AbstractUtil::ClassMethods#required_args?
      # @return [Array[Symbol]]
      def self.required_args
        %i[call code]
      end

      # use StandardError::initialize to assign kwargs to output
      def initialize(**kwargs)
        init_with_attrs(**kwargs)
        super("#{msg}:\n'#{call}'\nExited with code: #{code}")
      end
    end
  end
end
