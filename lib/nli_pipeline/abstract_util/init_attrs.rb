module NliPipeline
  # TODO: on v1.0.0 release, it may be possible to replace this with open struct
  # if path is no longer a required argument

  # Abstract Utilities for handling class instantiation
  # similar to open struct, but enforces some arguments
  module AbstractUtil
    # override include to automatically extend classmethods
    # i.e. functions in the module ClassMethods becom class methods
    # when the AbstractUtil is included
    def self.included(parent)
      parent.extend(ClassMethods)
    end

    # @raise [ArgumentError] unless all values in kwargs are truthy
    # @return [Boolean]
    def raise_unless_all(**kwargs)
      message = "All #{kwargs} must be truthy to use"\
        " #{self.class}.#{caller_locations(1, 1)[0].label}"
      raise ArgumentError, message unless kwargs.values.all?
      true
    end

    # keys become instance variable names
    # value is set using corresponding value in kwargs
    # all instance variables have getters, but not setters
    # add attributes to class
    def add_attrs(**kwargs)
      kwargs.each do |k, v|
        instance_variable_set("@#{k}", v)
        self.class.class_eval { attr_reader k.to_sym }
      end
      puts(to_s.yellow) if @debug
    end

    # support method for init_with_attrs to warn user about unused kwargs
    def drop_forbidden_args_message(**kwargs)
      forbidden_args = self.class.get_forbidden_kwargs(**kwargs)
      show_warning = !forbidden_args.empty? && kwargs[:debug]
      puts("Warning, dropping args #{forbidden_args} to #{self.class}".red) if show_warning
    end

    # similar to openstruct initialize
    # assigns all keyword arguments as instance variables
    # but uses attr_reader, not attr_accessor
    def init_with_attrs(**kwargs)
      sanitized_args = self.class.get_allowed_kwargs(**kwargs)
      drop_forbidden_args_message(**kwargs)
      unless self.class.required_args?(**kwargs)
        # raise SystemWrapper::ConfigError.new()
        msg = "#{self.class}.#{caller_locations(1, 1)[0].label}"\
          " requires arguments: #{self.class.required_args}"
        raise ArgumentError, msg
      end
      # merge passed args with default values for args
      merged_args = self.class.supported_args.merge(sanitized_args)
      add_attrs(**merged_args)
    end

    # override default to_s for nice formatting of instance vars
    def to_s
      format_instance_vars = lambda do |x, y|
        x + format("\n%<key>-30s: %<value>s", key: y, value: instance_variable_get(y.to_sym))
      end
      vars = instance_variables.reduce('', &format_instance_vars)
      "\n#{self.class} config#{vars}\n"
    end

    # methods to be loaded as static/class methods in class that extends AbstractUtil
    module ClassMethods
      # :nocov:
      # use method_missing to force classes that use AbstractUtil to implement
      #   supported_args and required_args.
      # @param method [String]
      # @param args   [Array]
      def method_missing(method, *args)
        # cast methods to array of symbols, compare to current missing method
        if %i[supported_args required_args].include?(method.to_sym)
          message = "Method #{method} must be implemented in #{self.class} "\
            'in order to use AbstractUtil'
          raise ArgumentError, message
        end
        super
      end
      # can't test call to super, so wrap in nocov tag
      # :nocov:

      # support for method_missing
      # https://robots.thoughtbot.com/always-define-respond-to-missing-when-overriding
      def respond_to_missing?(method, *args)
        %i[supported_args required_args].include?(method.to_sym) || super
      end

      # @return [Hash] args that can be converted into instance vars for class
      def get_allowed_kwargs(**kwargs)
        kwargs.select { |k, _v| supported_args.key?(k) }
      end

      # @return [Hash] args that can't be converted into instance vars for class
      def get_forbidden_kwargs(**kwargs)
        kwargs.reject { |k, _v| supported_args.key?(k) }
      end

      # @return [Boolean] have all required args been passed?
      def required_args?(**kwargs)
        required_args.all? { |key| kwargs.key?(key) }
      end
    end
  end
end
