require 'nli_pipeline/system_wrapper/call_wrapper'

module NliPipeline
  # simple class for managing git commands
  class GitManager < SystemWrapper::CallWrapper
    # @return [String] full url to commit
    def last_commit_url
      "#{remote}/commits/#{last_commit}"
    end

    # @param  upstream [String]
    # @return [String] url for git remote origin
    def remote(upstream: 'origin')
      call_system("git ls-remote --get-url #{upstream}", return_output: true)
    end

    # @return [String] full length hash of last commit
    def last_commit
      call_system("git log -1 --pretty=format:'%H'", return_output: true)
    end

    # @return [String] full messaage for last commit, all as one line
    def last_commit_message
      call_system('git log -1 --format=%B --oneline', return_output: true)
    end

    # @return [String] current branch
    def branch
      call_system('git symbolic-ref --short HEAD', return_output: true)
    end
  end
end
