# import all ruby files in nli_pipeline
Dir["#{__dir__}/nli_pipeline/**/*.rb"].each do |file|
  require file
end

# used to defined constants and include requirements for /bin/setup_pipeline
module NliPipeline
end
