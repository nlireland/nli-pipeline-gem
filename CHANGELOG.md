## 0.2.0 / 10/07/2018
#### Features:
* docker 

#### New Commands:

* ```docker_build```           build a docker image (if docker_build_commit)
* ```docker_build_commit```    check if docker build command was in the last commit
* ```docker_build_and_save```  same as build, but save as tarfile if build was successful
* ```docker_deploy_branch```   push branch to dockerhub
* ```docker_deploy_master```   push latest with date to dockerhub
* ```docker_exec```            send commands to running container
* ```docker_save```             run a docker image
* ```docker_save```            save a docker image as a tarfile

#### New Flags:

* ```--commands=```      (commands to run on docker container)
* ```--fail-on-error```  (fail docker build if any command returns a non-zero code)
* ```--git-branch```     (override name of branch for docker_deploy_branch)
* ```--image```          (docker image name)
* ```--mount=```         (path to mount directory to on container)
* ```--ports=```         (ports to bind on docker container)
* ```--proxy=```         (set proxy for docker build behind nli proxy)
* ```--upstream-image``` (image name for dockerhub) (defaults to nli/$image if not set)

### Internals

1. Replace all require_relative with require, use gemspec for dependencies in Gemfile
1. Refactor file_manager to use NliPipeline:;System::CallWrapper
   which handles debugging of system commands
1. Refactor docker_manager to use git_manager
   so build command is testable
1. Refactor manager classes to use AbstractUtil (similar to openstruct, but with required params and filtering)

### Notes

1. Move to YARD for documentation, available at ```doc/_index.html```
1. Add code coverage report using SimpleCov available at ```coverage/index.html```


## 0.1.2 / 2018-06-08
#### Features:

* ```--help```               show help screen
* ```--show-commands```      show available commands
* ```--show-flags```         show available arguments
* ```show_last_created```    show file created by last setup_pipeline command

Slight improvement on doc and test coverage



## 0.1.1 / 2018-05-14
#### Bug Fixes:

* Include runtime and development dependencies



## 0.1.0 / 2018-05-11
#### Breaking changes:

* Remove ```--backup``` and ```--no-backup``` flags 
(should always backup if file already exists)


#### Features:

* Add color output
* Add ```--debug``` flag to show all system commands and config
* Add ```no_output`` option to suppress all output during tests


## 0.0.1 / 2018-05-11
#### Features:

* Add RDOC
* Add ```--version``` flag to show current version
* Add ```--backup``` flag and ```--no-backup``` to handle backups
* Add ```--extension``` flag to use custom example file extenstions
* Add ```undo``` option to load backup files from setup_pipeline executable
* Slightly better testing, still unfinished / not using fake file system


## 0.0.0 / 2018-04-26
#### Features:

* Basic setup_pipeline executable which moves all .EXAMPLE to non .EXAMPLE location
