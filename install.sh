#!/bin/bash

# remove old gemfiles
rm $(find ./ -name '*.gem')

# install dependencies
bundle install

if [[ $1 == "build" ]]; then
  bundle exec rubocop 
  echo "running tests and generating coverage"
  unset CI
  bundle exec rspec
  export CI='true'
  # generate docs, exclude test data and other reports
  echo "generating docs"
  bundle exec yard
else
  echo "skipping doc and coverage generation"
fi

# build new gem
gem build nli_pipeline.gemspec
# install new gem
gem install ./nli_pipeline-*.gem
