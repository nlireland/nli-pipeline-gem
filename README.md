[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/nlireland/nli-pipeline-gem.svg)](https://bitbucket.org/nlireland/nli-pipeline-gem/addon/pipelines/home)
[![Gem Version](https://badge.fury.io/rb/nli_pipeline.svg)](https://badge.fury.io/rb/nli_pipeline)


# Motivation

This gem was created as a single point for managing bitbucket pipelines at the NLI.  
Previously many scripts that do very similar things were added to the pipelines,  
and keeping them in sync and up to date was tedious.  
This gem is a product of those scripts with some extra debugging / error handling features.


# Use

This gem is designed to setup bitbucket pipelines.  
It is available on rubygems https://rubygems.org/gems/nli_pipeline  
Example scripts can be found in ```spec/fixtures/git_project/*.sh```


# Commands
    
```bash
# move all .EXAMPLE files in the current directory to the same path without .EXAMPLE in
setup_pipeline $PWD

# load one of the backups made during a setup_pipeline command
setup_pipeline $PWD undo

# show version
setup_pipeline --version

# show all system commands and config
setup_pipeline $PWD --debug

# use custom extension
setup_pipeline $PWD --extension=.some_custom_extension

# check if the last commit contained a build command
# a build command is any combination of 'docker' and 'build' in square brackets e.g. 
# [docker build]
# some general commit message [ build docker ]
# [dear lord please build the docker image] (although I wouldn't recommend this one)
setup_pipeline $PWD docker_build --image=$some-image-name

# this will always have --fail-on-error set,
# so if the last commit was not a build commit
# or the tag fails
# or the push fails
# an exception is thrown
setup_pipeline $PWD docker_deploy_branch --image=$some-image-name


# --fail-on-error allows pipelines to pass without building a docker image.
# If no build command is used, 
# for example: if the last commit was not a build commit
# the command below will finish without failing the build
setup_pipeline $PWD docker_build_and_save

# If the previous commit was a build commit,
# the command below will only finish without error.
setup_pipelien $PWD docker_build_and_save --fail-on-error
```


# Installation

1. Via rubygems
  
```bash
gem install nli_pipeline
```

1. Build

```bash
# remove any old versions of the gem
# build the new version
# build the docs
# install the new version
./install.sh

# you can pass build argument to build the docs and test coverage
./install.sh build

# run the following in irb to check the gem is working
require 'nli_pipeline'
```


# Testing


```bash
bundle exec rspec

# to skip generating coverage reports, set $CI
export CI=true

# unit tests
bundle exec rspec --tag ~file_system

# integration tests (tests that actually move files)
bundle exec rspec --tag file_system

# check code style
bundle exec rubocop
```


# Documentation
```bash
# generate documentation
bundle exec yard

# show undocumented methods
bundle exec yard stats --list-undoc
```


# System util dependencies

    1. git
    1. grep
    1. echo


# Deploy

```bash
# you may be prompted to enter an email and password
# you can only push the gem version if you have write access to the gem on rubygems
# make sure to update History.txt with your changes!
# and add a git tag that matches the gem version
gem push nli_pipeline-$YOUR_VERSION.gem
```
