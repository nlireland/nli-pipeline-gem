# require 'memfs'
require 'support/file_helper'
require 'support/shared_context'

# turn off coverage reports on CI
if !ENV['CI']
  require 'simplecov'
  SimpleCov.start do
    add_group 'System Wrapper', 'lib/nli_pipeline/system_wrapper'
    add_group 'Abstract Utility', 'lib/nli_pipeline/abstract_util'
    add_filter '/spec/'
  end
else
  puts('Skipping test coverage report. Unset $CI to run the report')
end

RSpec.configure do |config|
  config.add_setting :fixture_path
  config.fixture_path = "#{__dir__}/fixtures"

  config.before do
    # suppress puts output during test
    allow($stdout).to receive(:puts)
    # MemFs.activate!
  end

  config.after do
    #   MemFs.deactivate!
    # reset args if set in previous specs
    # unset all args after test
    ARGV.each_with_index { |v, i| ARGV[i] = nil if v }
  end

  config.include Helpers

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'
end
