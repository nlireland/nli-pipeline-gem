require 'spec_helper'
require 'nli_pipeline'

describe NliPipeline::DockerManager do
  subject { NliPipeline::DockerManager }
  before(:each) do
    @image_name = 'test_image'
    @tar_name = "#{@image_name}.tar"
    @custom_tar_name = 'test.tar'
    @upstream_image_name = 'nlirealnd/upstream_test_image'

    @ports = [80, 8080]
    @tag = 'test-tag'
    @default_vars_for_tags = { image_name: false, upstream_image_name: false }
    @git_branch = 'develop'

    @test_commands = %w[list of tests]
    @fixture_path = RSpec.configuration.fixture_path
    @dm = subject.new(path: @fixture_path, commit: false)
    @container_name = @dm.container_name
  end

  describe 'initialize' do
    it 'should have attr_reader for all args on initialize' do
      # test instance variables get set dynamically
      falsey_vals = %i[
        debug fail_on_error image_name upstream_image_name
        tar_name proxy
      ]
      falsey_vals.each { |m| expect(@dm.send(m)).to be false }

      arr_vals = %i[ports bash_commands]
      arr_vals.each { |m| expect(@dm.send(m)).to eq [] }
    end
    it 'should use image_name.tar as tarname if tarname is not set' do
      @dm = subject.new(path: @fixture_path, image_name: @image_name, commit: false)
      expect("#{@dm.image_name}.tar").to eq @dm.tar_name
    end
    it 'should allow tar_name to be set in initialize' do
      @dm = subject.new(path: @fixture_path, image_name: @image_name, tar_name: @custom_tar_name,
                        commit: false)
      expect(@dm.tar_name).to eq @custom_tar_name
    end
    context 'when upstream image is not set' do
      before do
        @dm = subject.new(path: @fixture_path, image_name: @image_name)
      end
      it 'should use the nlireland/imagename as upstream_image_name' do
        expect(@dm.upstream_image_name).to eq "nlireland/#{@dm.image_name}"
      end
    end
    context 'when upstream image is set' do
      before do
        @dm = subject.new(
          path: @fixture_path, image_name: @image_name,
          upstream_image_name: @upstream_image_name
        )
      end
      it "should use #{@upstream_image_name} as the upstream_image_name" do
        expect(@dm.upstream_image_name).to eq @upstream_image_name
        expect(@dm.upstream_image_name).to_not eq "nlireland/#{@dm.image_name}"
      end
    end
  end

  describe 'build_commit?' do
    context 'when the last commit has docker build command' do
      before do
        allow_any_instance_of(NliPipeline::GitManager)
          .to receive(:last_commit_message).and_return('[docker build]')
      end
      it 'should return true' do
        build_command_regex = %r{
              # match echo ''
              echo\s'(.*)'
              # match grep -o [.*]
              \s\|\sgrep\s-o\s'\\\[\.\*\\\]'
              # match grep -i docker
              \s\|\sgrep\s-i\s'docker'
              # match grep -i build
              \s\|\sgrep\s-i\s'build'
            }x
        expect(@dm).to receive(:system).with(build_command_regex).and_return(true)
        expect(@dm.build_commit?).to be true
      end
    end
    context 'when the last commit has no docker build command' do
      before do
        allow_any_instance_of(NliPipeline::GitManager)
          .to receive(:last_commit_message).and_return('no build')
      end
      it 'should return false' do
        outputs = [
          /can build docker(.*) \: false/
        ]
        outputs.each { |o| expect(STDOUT).to receive(:puts).with(o) }
        expect(@dm.build_commit?).to be false
      end
      context 'when fail-on-error is true' do
        before do
          @dm.instance_variable_set('@fail_on_error', true)
        end
        it 'should raise an error' do
          err = NliPipeline::SystemWrapper::CallWrapperError
          expect { @dm.build_commit? }.to raise_error(err)
        end
      end
    end
  end

  describe 'build' do
    context 'when it can not build' do
      before do
        allow(@dm).to receive(:build_commit?).and_return(false)
      end
      it 'should return false' do
        outputs = [
          # /can build docker(\s+)\: false/,
          ## build_commit? is stubbed, so this won't hit stdout
          /built docker image(\s+)\: false/,
          'Skipping build'.yellow
        ]
        outputs.each { |o| expect(STDOUT).to receive(:puts).with(o) }
        expect(@dm.build).to be false
      end
    end
    context 'when it can build' do
      before do
        allow_any_instance_of(subject).to receive(:build_commit?).and_return(true)
      end
      context 'when it has an image name' do
        before do
          # give docker manager image name and path
          @dm = subject.new(path: @fixture_path, image_name: @image_name, commit: false)
        end
        it 'should build and tag the docker image' do
          expect(@dm).to receive(:system).with("docker build -t #{@image_name} #{@fixture_path}")
          @dm.build
        end
      end
      context 'when it does not have image name' do
        it 'should build the docker image only' do
          expect(@dm).to receive(:system).with("docker build #{@fixture_path}")
          @dm.build
        end
      end
      context 'when it has a proxy' do
        before do
          @test_proxy = 'http://test.proxy'
          @dm = subject.new(path: @fixture_path, proxy: @test_proxy, commit: false)
        end
        it 'should include the proxy as a build argument' do
          output = "docker build --build-arg proxy=#{@test_proxy} #{@fixture_path}"
          expect(@dm).to receive(:system).with(output)
          @dm.build
        end
      end
      context 'when it has a commit' do
        before do
          # is true by default, but makes command harder to test
          # since the argument sent to system becomes very long
          # and complicated to regex match for correctly
          @dm = subject.new(path: @fixture_path, commit: true)
        end
        it 'should include the full url to the commit as a build argument' do
          git_commit_regex = /(?:git|ssh|https?|git@[-\w.]+):(\/\/)?(.*?)\/commits\/(\w+)/
          full_regex = /docker build --build-arg commit=#{git_commit_regex.source} #{@fixture_path}/
          expect(@dm).to receive(:system).with(full_regex)
          @dm.build
        end
      end
    end
  end

  describe 'build_and_save' do
    context 'when it can not build' do
      before do
        allow(@dm).to receive(:build_commit?).and_return(false)
      end
      it 'should return false' do
        expect(@dm.build_and_save).to be false
      end
    end
    context 'when it has built' do
      before do
        allow_any_instance_of(subject).to receive(:build).and_return(true)
      end
      context 'when it has image name' do
        before do
          @dm = subject.new(path: @fixture_path, image_name: @image_name, tar_name: @tar_name,
                            commit: false)
        end
        it 'should build, tag, and save the docker image' do
          output = "docker save #{@image_name} -o #{@fixture_path}/#{@tar_name}"
          expect(@dm).to receive(:system).with(output)
          @dm.build_and_save
        end
      end
    end
  end

  describe 'load' do
    before do
      @dm = subject.new(path: @fixture_path, image_name: @image_name, commit: false)
    end
    it 'should send the load command to the docker service' do
      expect(@dm).to receive(:system).with("docker load -i #{@fixture_path}/#{@image_name}.tar")
      @dm.load
    end
  end

  describe 'run' do
    context 'when it has no build commit' do
      before do
        allow_any_instance_of(subject).to receive(:build_commit?).and_return(false)
      end
      context 'when it has no upstream image name' do
        it 'should raise a config error' do
          message = 'you must set image_name or upstream_image_name to run a docker image'
          error_mesage_regex = /Config Error\: #{message}/
          expect do
            @dm.run
          end.to raise_error(NliPipeline::SystemWrapper::ConfigError, error_mesage_regex)
        end
      end
      context 'when it has an upstream image name' do
        context 'when it has ports' do
          it 'should run the upstream image with ports bound' do
            config = {
              path: @fixture_path, upstream_image_name: @upstream_image_name,
              ports: @ports
            }
            local_dm = subject.new(config)
            output = 'docker run -id -p 80:80 -p 8080:8080 -e CI=TRUE --name'\
              " #{@container_name} #{@upstream_image_name}"
            expect(local_dm).to receive(:system).with(output)
            local_dm.run
          end
        end
        context 'when it has no ports' do
          it 'should run the upstream image' do
            local_dm = subject.new(path: @fixture_path, upstream_image_name: @upstream_image_name)
            output = "docker run -id -e CI=TRUE --name #{@container_name} #{@upstream_image_name}"
            expect(local_dm).to receive(:system).with(output)
            local_dm.run
          end
        end
      end
    end
    context 'when it has a build commit' do
      before do
        allow_any_instance_of(subject).to receive(:build_commit?).and_return(true)
      end
      it 'should load an run the local image' do
        local_dm = subject.new(path: @fixture_path, image_name: @image_name, commit: false)
        expect(local_dm).to receive(:load)
        output = "docker run -id -e CI=TRUE --name #{@container_name} #{@image_name}"
        expect(local_dm).to receive(:system).with(output)
        local_dm.run
      end
    end
  end

  describe 'docker_exec' do
    before do
      @dm.instance_variable_set(:@bash_commands, @test_commands)
    end
    it 'should run each command passed to it' do
      @test_commands.each do |cmd|
        expect(@dm).to receive(:system).with("docker exec #{@container_name} bash -c '#{cmd}'")
      end
      @dm.docker_exec
    end
  end

  describe 'tag' do
    context 'when image_name or upstream_image_name are not set' do
      it 'should raise an error' do
        regex = /All #{@default_vars_for_tags} must be truthy to use #{subject}\.tag/
        expect { @dm.send(:tag, @tag) }.to raise_error(ArgumentError, regex)
      end
    end
    context 'when both image_name and upstream_image_name are set' do
      it 'should tag the docker image' do
        config = {
          path: @fixture_path, image_name: @image_name,
          upstream_image_name: @upstream_image_name
        }
        local_dm = subject.new(config)
        output = "docker tag #{@image_name} #{@upstream_image_name}:#{@tag}"
        expect(local_dm).to receive(:system).with(output)
        local_dm.send(:tag, @tag)
      end
    end
  end

  describe 'deploy_master' do
    context 'when the last commit was a build commit' do
      before do
        allow_any_instance_of(subject).to receive(:build_commit?).and_return true
      end
      context 'when image_name or upstream_image_name are not set' do
        it 'should raise an error' do
          regex = /All #{@default_vars_for_tags} must be truthy to use #{subject}\.tag/
          expect { @dm.deploy_master }.to raise_error(ArgumentError, regex)
        end
      end
      context 'when both image_name and upstream_image_name are set' do
        it 'should tag the image as latest and latest with date' do
          config = {
            path: @fixture_path, image_name: @image_name,
            upstream_image_name: @upstream_image_name,
            commit: false
          }
          local_dm = subject.new(config)
          # allow(local_dm).to receive(:system).and_return(true)
          # account for latest and latest with date
          outputs = [
            /docker tag #{@image_name} #{@upstream_image_name}:latest/,
            "docker push #{@upstream_image_name}:latest",
            /docker tag #{@image_name} #{@upstream_image_name}:latest([-\d]+)/,
            /docker push #{@upstream_image_name}:latest([-\d]+)/
          ]
          outputs.each { |o| expect(local_dm).to receive(:system).with(o) }
          local_dm.deploy_master
        end
      end
    end
  end

  describe 'deploy_branch' do
    context 'when the last commit was a build commit' do
      before do
        allow_any_instance_of(subject).to receive(:build_commit?).and_return true
      end
      context 'when image_name or upstream_image_name are not set' do
        it 'should raise an error' do
          default_vars_for_branch = { image_name: false, upstream_image_name: false }
          # git branch will be set automatically, so the tag step will fail
          # which in turn fails the deploy step
          regex = /All #{default_vars_for_branch} must be truthy to use #{subject}\.tag/
          expect { @dm.deploy_branch }.to raise_error(ArgumentError, regex)
        end
      end
      context 'when both image_name and upstream_image_name are set' do
        it 'should tag the image with the branch name' do
          config = {
            path: @fixture_path, image_name: @image_name,
            upstream_image_name: @upstream_image_name, git_branch: @git_branch
          }
          local_dm = subject.new(config)
          outputs = [
            /docker tag #{@image_name} #{@upstream_image_name}:#{@git_branch}/,
            "docker push #{@upstream_image_name}:#{@git_branch}"
          ]
          outputs.each { |o| expect(local_dm).to receive(:system).with(o) }
          local_dm.deploy_branch
        end
      end
    end
  end
end
