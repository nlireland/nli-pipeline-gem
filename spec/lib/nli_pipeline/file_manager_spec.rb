require 'spec_helper'
require 'nli_pipeline'

# TODO: use fake file system to test core functionality properly

describe NliPipeline::FileManager do
  subject { NliPipeline::FileManager }
  before(:each) do
    # @fixture_path = "#{File.dirname(__dir__)}/../fixtures"
    @fixture_path = RSpec.configuration.fixture_path
    @log_dir = "#{@fixture_path}/.pipeline_logs"
    @log_content = "#{@fixture_path}/test.EXAMPLE"
    @fm = subject.new(path: @fixture_path)
  end

  describe 'initialize' do
    # defaults
    context 'when no value is specified' do
      it 'should backup files and look for .EXAMPLE files' do
        # fm.backup.should === true
        expect(@fm.extension).to eq('.EXAMPLE')
        expect(@fm.debug).to be false
      end
    end

    it 'should use the path as the basis for the backup path' do
      expect(@fm.backup_path).to eq "#{@fm.path}/**/*.backup*"
    end
  end

  describe 'all_example_files' do
    context 'when example files exist' do
      it 'should find all example files' do
        expect(@fm.all_example_files).to eq(["#{@fixture_path}/test.EXAMPLE"])
      end
    end
    context 'when custom example file extension is set' do
      it 'should find all example files with that extension' do
        @fm.instance_variable_set('@extension', '.fake')
        expect(@fm.all_example_files).to eq(["#{@fixture_path}/test3.example.fake"])
      end
    end
    context "when example files don't exist" do
      it 'should raise an error' do
        @fm.instance_variable_set('@extension', '.EXTENSION_THAT_DOES_NOT_EXIST')
        expect { @fm.all_example_files }.to raise_error(ArgumentError)
      end
    end
  end

  describe 'copy_example_files', file_system: true do
    context 'when no non-example files exist' do
      it 'should copy example files to the right location' do
        output_string = "cp #{@fixture_path}/test.EXAMPLE #{@fixture_path}/test"
        expect(@fm).to receive(:system).with(output_string)
        @fm.copy_example_files
      end
    end
    context 'when non-example files exist' do
      before do
        file_content = 'this file should only exist during rspec tests'
        add_test_file("#{@fixture_path}/test", 'w', file_content)
      end
      after do
        remove_test_file("#{@fixture_path}/test")
        clear_pipeline_logs(@log_dir)
        ## this does the same as the block above
        ## but relying on one method to test another on the same subject is bad
        # @fm.last_created_files().each do |to_delete|
        #   puts to_delete
        #   File.delete("#{to_delete}")
        # end
      end

      it 'should backup the old files before copying the example files' do
        output_string = "cp #{@fixture_path}/test.EXAMPLE #{@fixture_path}/test"
        expect(@fm).to receive(:system).with(output_string)
        output_regex = /cp #{@fixture_path}\/test #{@fixture_path}\/test\.backup[0-9]*/
        expect(@fm).to receive(:system).with(match(output_regex))
        @fm.copy_example_files
      end
    end
  end

  describe 'last_created_files', file_system: true do
    context 'when the logs are empty' do
      before do
        clear_pipeline_logs(@log_dir)
      end
      it 'should warn the user and return false' do
        expect(STDOUT).to receive(:puts).with(/No logs found in #{@log_dir}/)
        expect(@fm.last_created_files).to eq false
      end
    end
    context 'when the logs are not empty' do
      before do
        Dir.mkdir(@log_dir) unless Dir.exist? @log_dir
        add_test_file("#{@log_dir}/created_files_201806221656", 'w', @log_content)
      end
      after do
        remove_test_file("#{@log_dir}/created_files_201806221656")
      end
      it 'should return the contents of the latest log file' do
        # File.readlines returns contents as array
        expect(@fm.last_created_files).to eq [@log_content]
      end
    end
  end

  describe 'all_backups', file_system: true do
    # context 'when no backups exist', :backup => false do
    context 'when no backups exist' do
      include_context 'no backup'
      it 'should return an empty list' do
        expect(@fm.all_backups).to eq []
      end
    end
    context 'when backups exist' do
      include_context 'backup'
      it 'should return the backup dates from newest to oldest' do
        backups = @fm.all_backups
        backups.each do |date|
          expect(date).to match(/\d+/)
        end
      end
    end
  end

  describe 'load_from_backup', file_system: true do
    context 'when no backups exist' do
      include_context 'no backup'
      it 'should warn the use and return false' do
        expect(STDOUT).to receive(:puts).with(/loading backup in #{@fixture_path}/)
        expect(STDOUT).to receive(:puts).with(/No backups found in .*/)
        expect(@fm.load_from_backup).to eq false
      end
    end

    context 'when backups exist' do
      include_context 'backup'
      before do
        @first_backup_date = @fm.all_backups[0]
        @copy_backup_regex = /cp (.*)\.backup(.*) (.*)/
      end
      it 'should load the backup selected by the user' do
        # first_backup = Dir.glob("#{@fixture_path}/**/*.backup*")[0]
        allow(@fm).to receive(:handle_backup_user_input).and_return(@first_backup_date)
        expect(STDOUT).to receive(:puts).with(/loading backup in #{@fixture_path}/)
        @fm.load_from_backup
      end
      it 'should copy the selected backup files' do
        allow(STDIN).to receive_message_chain(:gets, :chomp) { @first_backup_date }
        expect(@fm).to receive(:system).with(@copy_backup_regex)
        @fm.load_from_backup
      end
      context 'when the user selects an invalid backup' do
        it 'should keep asking for input until a valid backup is selected' do
          backup_date = 'no_backup'
          # return bad date the first tiome, then good date
          allow($stdin).to receive_message_chain(:gets, :chomp)
            .and_return(backup_date, @first_backup_date)
          outputs = [
            /loading backup in #{@fixture_path}/,
            /which backup would you like to load\?(.*)/,
            "#{backup_date} is not in backups".red,
            /please choose from: (.*)/
          ]

          outputs.each do |output|
            expect(STDOUT).to receive(:puts).with(output)
          end
          expect(@fm).to receive(:system).with(@copy_backup_regex)
          @fm.load_from_backup
        end
      end
    end
  end
end
