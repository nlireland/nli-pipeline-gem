require 'spec_helper'
require 'nli_pipeline'

describe NliPipeline::SystemWrapper::ConfigError do
  subject { NliPipeline::SystemWrapper::ConfigError }
  describe 'initialize' do
    context 'when no config is passed' do
      it 'should raise an ArgumentError' do
        expect { subject.new }.to raise_error(ArgumentError)
      end
    end
    context 'when config is passed' do
      before do
        @config = { bad_key: 'bad_value' }
        @file = 'test.txt'
        @msg = 'custom message'
        @ce = subject.new(config: @config, file: @file, msg: @msg)
      end
      it 'should have config' do
        expect(@ce.config).to eq @config
      end
      it 'should raise a custom message' do
        expect { raise @ce }.to raise_error("#{@msg}: #{@config} in #{@file}")
      end
    end
  end
end
