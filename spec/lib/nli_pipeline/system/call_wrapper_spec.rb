require 'spec_helper'
require 'nli_pipeline'

describe NliPipeline::SystemWrapper::CallWrapper do
  subject { NliPipeline::SystemWrapper::CallWrapper }
  describe 'call_system' do
    context 'when it fails on error' do
      before(:each) do
        # debug: false, fail_on_error: false
        @cs = subject.new(debug: false, fail_on_error: true)
      end
      it 'should raise a CallWrapperError' do
        expect do
          @cs.call_system("echo 'hi' | grep 'y'")
        end.to raise_error(NliPipeline::SystemWrapper::CallWrapperError)
      end
      it 'should print a custom error message if passed' do
        custom_error_message = 'custom error'
        expect(STDOUT).to receive(:puts).with(custom_error_message.red)
        expect do
          @cs.call_system("echo 'hi' | grep 'y'", custom_error_message: custom_error_message)
        end.to raise_error(NliPipeline::SystemWrapper::CallWrapperError)
      end
    end
    context 'when it does not fail on error' do
      before(:each) do
        # debug: false, fail_on_error: false
        @cs = subject.new(debug: false, fail_on_error: false)
      end
      it 'should not raise any error' do
        # command should finish, but should return false
        # (since grep returns non-zero code if no match is found)
        expect(@cs.call_system("echo 'hi' | grep 'y'")).to be false
      end
    end
  end

  describe 'pretty_print' do
    before(:each) do
      # debug: false, fail_on_error: false
      @cs = subject.new(debug: false, fail_on_error: false)
    end
    it 'should return red output if the block passed returns a falsey value' do
      expect(STDOUT).to receive(:puts).with('false'.red)
      expect(@cs.send(:pretty_print) { false }).to be false
    end
    it 'should return green output if the block passed returns a truthy value' do
      expect(STDOUT).to receive(:puts).with('true'.green)
      expect(@cs.send(:pretty_print) { true }).to be true
    end
    it 'should prepend a custom message if passed' do
      send_args = { custom_message: 'custom message' }
      format_args = send_args.merge(ret: true)
      full_message = format('%<custom_message>-20s: %<ret>s', format_args).green

      expect(STDOUT).to receive(:puts).with(full_message)
      expect(@cs.send(:pretty_print, send_args) { true }).to be true
    end
  end
end
