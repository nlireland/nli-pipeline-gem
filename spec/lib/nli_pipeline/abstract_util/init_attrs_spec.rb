require 'spec_helper'
require 'nli_pipeline'

class BrokenClass
  include NliPipeline::AbstractUtil
end

class WorkingClass
  include NliPipeline::AbstractUtil

  def self.required_args
    [:test]
  end

  def self.supported_args
    { test: '', other_test: 'some default value' }
  end

  def initialize(**kwargs)
    init_with_attrs(**kwargs)
  end
end

describe NliPipeline::AbstractUtil do
  describe 'init_with_attrs' do
    context 'when required methods are not implemented' do
      let(:subject) { BrokenClass }
      it 'should raise an error' do
        regex = /Method (\w+) must be implemented in (\w+) in order to use AbstractUtil/
        expect { subject.new.init_with_attrs }.to raise_error(ArgumentError, regex)
        # subject.new.init_with_attrs
      end
    end
    context 'when all required methods are implemented' do
      let(:subject) { WorkingClass }

      # should load the methods in the ClassMethods module as class methods
      it { expect(subject).to respond_to :get_allowed_kwargs }
      it { expect(subject).to respond_to :get_forbidden_kwargs }
      it { expect(subject).to respond_to :required_args? }
      context 'when args are passed to initialize' do
        before do
          @test = 't1'
          @other_test = 't2'
          @wc = subject.new(test: @test, other_test: @other_test)
        end
        it 'should assign key word agruments sent to init_with_attrs as instance variables' do
          expect(@wc.test).to eq @test
          expect(@wc.other_test).to eq @other_test
        end
      end
      context 'when no args are passed to initialize' do
        it 'should raise an error if the required argument(s) are missing' do
          message = "#{subject}.initialize requires arguments: #{subject.required_args}"
          expect { subject.new }.to raise_error(ArgumentError, message)
        end
      end
      context 'when the required arguments are passed' do
        it 'should use the defaults from supported_args unless overridden' do
          wc = subject.new(test: 'test')
          expect(wc.test).to eq 'test'
          expect(wc.other_test).to eq 'some default value'
        end
      end
    end
  end
  describe 'to_s' do
    let(:subject) { WorkingClass }

    it 'should format attrs output forto string' do
      kwargs = { test: 'test', other_test: 'other test' }
      header = "#{subject} config"
      attrs = kwargs.map { |k, v| "\\@#{k}(\\s+)\\:\\s#{v}" }
      regex = Regexp.new([header, *attrs].join('(\\s+)'))
      # regex = /#{subject} config(\s+)\@test(\s+)\:\stest(\s+)\@other_test(\s+)\:\sother test/
      expect(subject.new(**kwargs).to_s).to match regex
    end
  end

  describe 'raise_unless_all' do
    let(:subject) { WorkingClass.new(test: 'test') }
    it 'should raise an error unless all values are truthy' do
      expect { subject.raise_unless_all(one: true, two: false) }.to raise_error(ArgumentError)
    end
    it 'should return true is all values are truthy' do
      expect(subject.raise_unless_all(one: true, two: true)).to be true
    end
  end

  describe 'method_missing' do
    let(:subject) { WorkingClass.new(test: 'test') }
    @required_methods = %i[supported_args required_args]
    it 'should raise an error for undefined methods not handled by method_missing' do
      expect { subject.to_i }.to raise_error(NoMethodError)
    end
  end
end
