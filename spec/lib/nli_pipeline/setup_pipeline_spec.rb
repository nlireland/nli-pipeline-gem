require 'spec_helper'
require 'nli_pipeline'

# TODO: split into separate files to handle filemanager, dockermanager etc.
# TODO: use ARGV.push once order of flags doesn't matter
# TODO: test all combinations of flags
# test that args for DockerManager don't get passed to FileManager and vice versa
# handle argument passing better

describe NliPipeline::SetupPipeline do
  subject { NliPipeline::SetupPipeline.new }
  before(:each) do
    @fixture_path = RSpec.configuration.fixture_path
    @branch = 'test'
    @upstream = 'fake-upstream'
    @image = 'test-image'
    @dm = NliPipeline::DockerManager
    @latest_date_regex = /latest-[\d-]+/
    @bash_commands = %w[pwd ls]
    @container_name = 'pipeline-container'
    @ports = [1, 2]
    @proxy = 'http://test.proxy.com:80'
  end
  describe 'parse_args' do
    context 'when no arguments are passed' do
      before do
        ARGV[0] = ''
      end
      it 'should raise an error' do
        expect { subject.parse_args }.to raise_error(ArgumentError)
      end
    end
    context "when a directory that doesn't exist is passed" do
      before do
        ARGV[0] = "directory/that/doesn't/exist"
      end
      it 'should raise an error' do
        expect { subject.parse_args }.to raise_error(ArgumentError)
      end
    end
    context 'when a valid directory is passed as the first argument' do
      before do
        ARGV[0] = RSpec.configuration.fixture_path
      end
      it 'should parse the first arg as the path' do
        expect(subject.parse_args).to eq path: @fixture_path
      end

      context 'when the show_last_created command is used' do
        before do
          @command = 'show_last_created'
          ARGV[1] = @command
        end
        it 'should call last_created_files' do
          # expect_any_instance_of(NliPipeline::FileManager).to receive(:last_created_files)
          expect(subject.parse_args).to eq(path: @fixture_path, command: @command.to_sym)
        end
      end
    end
  end

  describe 'parse_flags' do
    # keep legacy test to ensure that --version does use the correct version
    context 'when the --version flag is passed' do
      before do
        ARGV[0] = '--version'
      end
      it 'should return the version and return false' do
        expect(STDOUT).to receive(:puts).with(NliPipeline::VERSION)
        # should return false and exit early
        # expect{pl.parse_flags()}.to output(NliPipeline::VERSION).to_stdout
        expect(subject.parse_flags).to eq false
      end
    end

    # handle all exit early flags
    funcs_and_flags = {
      help:          '--help',
      show_commands: '--show-commands',
      options:       '--show-flags',
      version:       '--version'
    }

    funcs_and_flags.each do |func, flag|
      context "when the #{flag} flag is passed" do
        before { ARGV[0] = flag }
        it "should call #{func} and exit early" do
          expect(subject).to receive(func)
          expect(subject.parse_flags).to eq false
        end
      end
    end
  end

  describe 'show_commands' do
    it 'should show all manager commands' do
      values = [
        'FileManager commands',
        %i[undo show_last_created],
        'DockerManager commands',
        %i[
          docker_build
          docker_build_commit
          docker_build_and_save
          docker_deploy_branch
          docker_deploy_master
          docker_exec
          docker_run
          docker_save
        ]
      ]

      values.each do |v|
        expect(STDOUT).to receive(:puts).with(v)
      end

      subject.show_commands
    end
  end

  describe 'main' do
    # duplicate of parse_flags
    # but is required since parse args throws an error
    # if first arg is not a directory
    # need to ensure that if parse_flags returns flase, main breaks early with no errors
    context 'when a flag that exits early is used' do
      context 'when the --version flag is passed' do
        before do
          ARGV[0] = '--version'
        end
        it 'should return the version' do
          expect(STDOUT).to receive(:puts).with(NliPipeline::VERSION)
          subject.main
        end
      end
      context 'when the --show-commands flag is passed' do
        before do
          ARGV[0] = '--show-commands'
        end
        it 'should show the available commands' do
          expect(subject).to receive(:show_commands)
          subject.main
        end
      end
      context 'when the --help flag is passed' do
        before do
          ARGV[0] = '--help'
        end
        it 'should show the user a help dialogue' do
          expect(STDOUT).to receive(:puts).with(/HOW TO USE\:(.*)/)
          subject.main
        end
      end
    end

    context 'when an invalid flag is passed' do
      before do
        @bad_flag = '--flag-that-doesnot-exist'
        ARGV[0] = @bad_flag
      end
      it 'should show the valid flags before raising the error' do
        # usage_header = /Usage\:\ssetup\_pipeline\s\[options\](.*)/
        expect(STDOUT).to receive(:puts).with("invalid option: #{@bad_flag}".red)
        # expect(STDOUT).to receive(:puts).with(usage_header)
        expect { subject.main }.to raise_error(OptionParser::InvalidOption)
      end
    end

    context 'when a valid directory is passed as the first argument' do
      before do
        ARGV[0] = @fixture_path
      end
      context 'when the --extension flag is passed' do
        before do
          @ext = '.TestExtension'
          ARGV[1] = "--extension=#{@ext}"
        end
        # no .TestExtension files should exist, so this will fail
        # ToDo: ensure no .TestExtansion file will ever exist at this point
        # possibly with fakefs?
        it "should pass the #{@ext} extension to FileManager but raise an error" do
          err_msg = "No #{@ext} Files found at #{@fixture_path}/**/*#{@ext}"
          expect { subject.main }.to raise_error(ArgumentError, err_msg)
        end
      end

      context 'when the show_last_created command is used' do
        before do
          ARGV[1] = 'show_last_created'
        end
        it 'should call last_created_files' do
          expect_any_instance_of(NliPipeline::FileManager).to receive(:last_created_files)
          subject.main
        end
      end
      context 'when the undo command is used' do
        before do
          ARGV[1] = 'undo'
        end
        it 'should call load_from_backup' do
          expect_any_instance_of(NliPipeline::FileManager).to receive(:load_from_backup)
          subject.main
        end
      end
      # TODO: make share context/step for checking command calls method
      context 'when the docker_save command is used' do
        before do
          ARGV[1] = 'docker_save'
        end
        it 'should call save on DockerManager' do
          expect_any_instance_of(NliPipeline::DockerManager).to receive(:save)
          subject.main
        end
      end
      context 'when the docker_build_commit command is used' do
        before do
          ARGV[1] = 'docker_build_commit'
        end
        it 'should check whether the last commit had a build command in it' do
          expect_any_instance_of(NliPipeline::DockerManager).to receive(:build_commit?)
          subject.main
        end
        context 'when --fail-on-error is passed' do
          before { ARGV[2] = '--fail-on-error' }
          context 'when the last commit was not a build commit' do
            before do
              allow_any_instance_of(NliPipeline::GitManager)
                .to receive(:last_commit_message).and_return('no build')
            end
            it 'should raise an error is the last commit was not a build commit' do
              err = NliPipeline::SystemWrapper::CallWrapperError
              expect { subject.main }.to raise_error(err)
            end
          end
          context 'when the last commit was a build commit' do
            before do
              allow_any_instance_of(NliPipeline::GitManager)
                .to receive(:last_commit_message).and_return('[build docker]')
            end
            it 'should return true' do
              @dm = NliPipeline::DockerManager
              expect_any_instance_of(@dm).to receive(:build_commit?).and_return(true)
              subject.main
            end
          end
        end
        context 'when the debug flag is passed' do
          before do
            ARGV[2] = '--debug'
          end
          it 'should show all system commands' do
            @dm = NliPipeline::DockerManager.new(path: @fixture_path, debug: true)
            build_command_regex = %r{
              (\\t|\s)+echo\s'(.*)' # match echo ''
              \s\|\sgrep\s-o\s'\\\[\.\*\\\]' # match grep -o [.*]
              \s\|\sgrep\s-i\s'docker' # match grep -i docker
              \s\|\sgrep\s-i\s'build' # match grep -i build
            }x

            outputs = [
              @dm.to_s.yellow,
              build_command_regex,
              /returned(.*)/,
              /can build docker(.*)/
            ]
            outputs.each { |o| expect(STDOUT).to receive(:puts).with(o) }
            subject.main
          end
        end
      end
      context 'when the docker_build command is used' do
        before do
          ARGV[1] = 'docker_build'
        end
        it 'should run the build method' do
          expect_any_instance_of(NliPipeline::DockerManager).to receive(:build)
          subject.main
        end
        context 'when the last commit was a build commit' do
          before do
            allow_any_instance_of(@dm).to receive(:build_commit?).and_return(true)
          end
          context 'when the --proxy flag has a value' do
            before do
              ARGV[2] = "--proxy=#{@proxy}"
            end
            it 'should pass the proxy as a build arg' do
              expect_any_instance_of(NliPipeline::DockerManager).to receive(
                :call_system
              ).with(/docker build --build-arg proxy=#{@proxy} (.*)/)
              subject.main
            end
          end
        end
      end
      context 'when the docker_run command is used' do
        before do
          ARGV[1] = 'docker_run'
        end
        context 'when the last commit was a build commit' do
          before do
            allow_any_instance_of(@dm).to receive(:build_commit?).and_return(true)
          end
          it 'should run the docker run method' do
            expect_any_instance_of(NliPipeline::DockerManager).to receive(:run)
            subject.main
          end
          context 'when the --mount and --image flag is passed' do
            before do
              ARGV[2] = '--mount=/test_workdir'
              ARGV[3] = "--image=#{@image}"
            end
            it 'should mount the volume on the docker container' do
              expected_outputs = [
                "docker load -i #{@fixture_path}/#{@image}.tar",
                /docker run(.*) -v (.*)/
              ]

              expected_outputs.each do |o|
                expect_any_instance_of(@dm).to receive(:system).with(o)
              end
              subject.main
            end
            context 'when the --ports flag has values' do
              before do
                ARGV[4] = "--ports=#{@ports.join(',')}"
              end
              it 'should bind ports on the running container' do
                expected_outputs = [
                  "docker load -i #{@fixture_path}/#{@image}.tar",
                  /docker run (.*) -p 1:1 -p 2:2 (.*)/
                ]

                expected_outputs.each do |o|
                  expect_any_instance_of(@dm).to receive(:system).with(o)
                end
                subject.main
              end
            end
          end
        end
      end
      context 'when the docker_build_and_save command is used' do
        before do
          ARGV[1] = 'docker_build_and_save'
        end
        it 'should run build_and_save' do
          expect_any_instance_of(NliPipeline::DockerManager).to receive(:build_and_save)
          subject.main
        end
      end
      context 'when the docker_exec argument is passed' do
        before do
          ARGV[1] = 'docker_exec'
          ARGV[2] = "--commands=#{@bash_commands.join(',')}"
        end
        it 'should run docker exec' do
          # expect_any_instance_of(NliPipeline::DockerManager).to receive(:docker_exec)
          @bash_commands.each do |cmd|
            expect_any_instance_of(NliPipeline::DockerManager).to receive(
              :call_system
            ).with("docker exec #{@container_name} bash -c '#{cmd}'")
          end
          subject.main
        end
      end
      context 'when the docker_deploy_master argument is passed' do
        before do
          ARGV[1] = 'docker_deploy_master'
        end
        context 'when the last commit was a build commit' do
          before do
            allow_any_instance_of(@dm).to receive(:build_commit?).and_return true
          end
          context 'when the upstream-image flag is set' do
            before do
              ARGV[2] = "--image=#{@branch}"
            end
            it 'should tag the image as latest and latest-date' do
              expect_any_instance_of(NliPipeline::DockerManager).to receive(:tag).with('latest')
              expect_any_instance_of(NliPipeline::DockerManager).to receive(:tag).with(
                @latest_date_regex
              )
              tags = ['latest', @latest_date_regex]
              tags.each do |t|
                push_command = /docker push nlireland\/#{@branch}\:#{t}/
                expect_any_instance_of(
                  NliPipeline::DockerManager
                ).to receive(:call_system).with(push_command)
              end
              subject.main
            end
          end
        end
      end
      context 'when the docker_deploy_branch argument is passed' do
        before do
          ARGV[1] = 'docker_deploy_branch'
        end
        context 'when the image does not exist' do
          # make sure image doesn't exist in before hook?
          xit 'should raise an error'
        end
        context 'when the last commit was not a build commit' do
          xit 'should raise an error' do
            err = NliPipeline::SystemWrapper::CallWrapperError
            expect { subject.main }.to raise_error(err)
          end
        end
        context 'when the last commit was a build commit' do
          before do
            allow_any_instance_of(@dm).to receive(:build_commit?).and_return true
          end
          context 'when the image name is set' do
            before do
              ARGV[2] = " --image=#{@branch}"
            end
            context 'when the --git-branch flag is set' do
              before do
                ARGV[3] = "--git-branch=#{@branch}"
              end
              it "should use #{@branch} as the docker image tag" do
                expect_any_instance_of(NliPipeline::DockerManager).to receive(:tag).with(@branch)
                # expect(subject).to receive(:system).with()
                push_regex = /docker push (.*)/
                expect_any_instance_of(
                  NliPipeline::DockerManager
                ).to receive(:call_system).with(push_regex)
                subject.main
              end
              context 'when the upstream-image is set' do
                before do
                  ARGV[4] = "--upstream-image=#{@upstream}"
                end
                it 'should push to upstream image' do
                  expect_any_instance_of(NliPipeline::DockerManager).to receive(:tag).with(@branch)
                  # expect(subject).to receive(:system).with()
                  push_command = "docker push #{@upstream}:#{@branch}"
                  expect_any_instance_of(
                    NliPipeline::DockerManager
                  ).to receive(:call_system).with(push_command)
                  subject.main
                end
              end
            end
            context 'when the --git-branch flag is not set' do
              xit 'should use the current git branch'
            end
          end
          context 'when the image name is not set' do
            it 'should raise an error' do
              required_args = { image_name: false, upstream_image_name: false }
              err_msg = "All #{required_args} must be truthy " \
                'to use NliPipeline::DockerManager.tag'
              expect { subject.main }.to raise_error(ArgumentError, err_msg)
            end
          end
        end
      end
    end
  end

  # describe 'show_commands' do
  #   it 'should show the available commands' do
  #     expect(STDOUT)
  #     expect(subject.new(path:@fixture_path).show_commands()).to eq [:undo, :show_last_created]
  #   end
  # end
end
