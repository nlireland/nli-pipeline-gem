require 'spec_helper'
require 'nli_pipeline'

# TODO: use fake file system to test core functionality properly

describe NliPipeline::GitManager do
  subject { NliPipeline::GitManager.new }
  before(:each) do
    @fixture_path = RSpec.configuration.fixture_path
  end
  describe 'last_commit_url' do
    it 'should return a full url to the current commit' do
      git_commit_regex = /(?:git|ssh|https?|git@[-\w.]+):(\/\/)?(.*?)\/commits\/(\w+)/
      expect(subject.last_commit_url).to match git_commit_regex
    end
  end
end
