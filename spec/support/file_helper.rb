# this will become redundant once FakeFs or MemFs is implemented
module Helpers
  # Arguments
  #   p: (string) file path
  # Raises ArgumentError unless the path is under the test directory
  # Otherwise return the apsolute path
  def check_path(path)
    full_path = File.absolute_path(path)
    under_test_dir = full_path.start_with?(Dir.pwd)
    raise ArgumentError, "#{full_path} must be under #{Dir.pwd}" unless under_test_dir
    full_path
  end

  # esafely clear test pipeline logs
  def clear_pipeline_logs(log_dir_path)
    full_log_dir_path = check_path(log_dir_path)
    Dir.glob("#{full_log_dir_path}/*").each do |log_file|
      # this creates of the path is ineficient,
      # but better safe than sorry when deleting files
      remove_test_file(log_file)
      # File.delete(log_file)
    end
  end

  # safely remove files using glob pattern
  def remove_test_files_by_pattern(path, pattern)
    full_path = check_path(path)
    Dir.glob("#{full_path}#{pattern}").each do |file|
      remove_test_file(file)
    end
  end

  # safely remove test file
  def remove_test_file(file_path)
    full_file_path = check_path(file_path)
    File.delete(full_file_path)
  end

  # safely add and write to test file
  def add_test_file(file_path, mode = 'a', data = '')
    full_file_path = check_path(file_path)
    File.open(full_file_path, mode) { |f| f.write(data) }
  end
end
