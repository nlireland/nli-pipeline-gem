shared_context 'no backup' do
  before do
    remove_test_files_by_pattern(@fixture_path, '/**/*.backup*')
  end
end

shared_context 'backup' do
  before do
    2.times do
      subject.new(path: @fixture_path).copy_example_files
      sleep(1)
    end
  end

  after do
    remove_test_files_by_pattern(@fixture_path, '/**/*.backup*')
    remove_test_file("#{@fixture_path}/test")
  end
end
