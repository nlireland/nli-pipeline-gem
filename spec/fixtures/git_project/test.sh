#!/usr/bin/env bash

# run fresh each time
docker stop pipeline-container
docker rm pipeline-container
touch test_workdir/test.EXAMPLE

# flags="--debug -foe"

# handles checking if the last commit was a build commit
# if it was, build the image and save as taf
# if it wasn't throw an exception (-foe)
setup_pipeline $PWD docker_build_and_save --image='test-image' $flags

# if the last commit was a build commit, load the tar
# run the container, bind ports, mount the current directory to mount path
setup_pipeline $PWD docker_run --ports=80,8080 --image='test-image' \
  --mount='/test_workdir' $flags

# run commands on the container (will always be called pipeline-container)
setup_pipeline $PWD docker_exec --commands='setup_pipeline $PWD' $flags

# run fresh each time
rm test_workdir/test
