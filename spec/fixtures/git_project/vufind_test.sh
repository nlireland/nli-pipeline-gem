#!/usr/bin/env bash
# copy this script to the vufind directory to test it
# it will try to deploy the test tag to dockerhub!

# run fresh each time
docker stop pipeline-container
docker rm pipeline-container

flags="--debug"

# handles checking if the last commit was a build commit
# if it was, build the image and save as taf
# if it wasn't throw an exception (-foe)
# setup_pipeline $PWD/docker docker_build_and_save --image='test-image' $flags

# if the last commit was a build commit, load the tar
# run the container, bind ports, mount the current directory to mount path
setup_pipeline $PWD docker_run --ports=80,8080 --image='nli-vufind4' \
  --mount='/usr/local/vufind' $flags

# run commands on the container (will always be called pipeline-container)
setup_pipeline $PWD docker_exec \
  --commands='composer install','chmod 777 ./phing.ci.sh','./phing.ci.sh nli_unit_tests' $flags

setup_pipeline $PWD docker_deploy_branch --image='nli-vufind4' --git-branch='test'
